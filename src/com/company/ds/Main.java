package com.company.ds;


/**
 * Node is class to represent node of a tree of degree 2
 */
class Node{
    private Node left;
    private Node right;
    private Integer value;

    public Node(Integer value) {
        this.value = value;
    }

    boolean hasLeft(){
        return left != null;
    }

    boolean hasRight(){
        return right != null;
    }

    boolean isLeafNode(){
        return left == null && right == null;
    }

    public int getValue() {
        return value;
    }

    public Node getLeft() {
        return left;
    }

    public Node getRight() {
        return right;
    }


    public void setLeft(Node left) {
        this.left = left;
    }

    public void setRight(Node right) {
        this.right = right;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public boolean isEmpty(){
        return value == null;
    }
}

/*

1 -> 2  -> 3  -> null
4  ->  5 -> null/nil


 */

/*
             1                 // level 0
       2           3           // level 1
    4    5       6   7         // level 2


1.left = 2
1.right = 3

2.left = 4
2.right = 5


3.left = 6
3.right = 7


4,5,6,7 are leaf node with left and right as null

 */

/*
print value: 4 2 5 1 6 3 7


 */


/*
TODO:

Depth First Traversals:
1. Inorder (Left, Root, Right)
2. Preorder (Root, Left, Right) Q2
3. Postorder (Left, Right, Root) Q3

Breadth First or Level Order Traversal: question 1
 */


public class Main {
//             1                 // level 0
//       2           3           // level 1
//    4    5      6   7         // level 2

    public static void inorder(Node root) {
//        (Left, Root, Right)
//        4 2 5 1 6 3 7

        // Left
        if (root.hasLeft()){
            inorder(root.getLeft());
        }

        // Root
        System.out.print(root.getValue());
        System.out.print(", ");

        //Right
        if (root.hasRight()) {
            inorder(root.getRight());
        }
        
    }



    public static void main(String[] args) {
	// write your code here

//             1                 // level 0
//       2           3           // level 1
//    4    5       6   7         // level 2
        Node root = new Node(1);

        Node next = new Node(2);
        root.setLeft(next);
        Node next3 = new Node(3);
        root.setRight(next3);

        next.setLeft(new Node(4));
        next.setRight(new Node(5));

        next3.setLeft(new Node(6));
        next3.setRight(new Node(7));

        inorder(root);
    }
}
