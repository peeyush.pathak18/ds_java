package com.company.ds;

/*

leetcode solution for https://leetcode.com/problems/add-two-numbers/
 */
public class Add2Num {

  static public class ListNode {
        int val;
        ListNode next;
        ListNode() {}
        ListNode(int val) { this.val = val; }
        ListNode(int val, ListNode next) { this.val = val; this.next = next; }
    }




  static public class Solution {



    public ListNode add(ListNode l1, ListNode l2, int carry, ListNode prev, ListNode root) {
      if (l1 == null && l2 == null){
        return root;
      }

      ListNode current;
      int result = 0;
      if (l1 == null && l2 != null) {
        result = l2.val + carry;
      } else if (l2 == null && l1 != null) {
        result = l1.val + carry;
      } else {
        result = l1.val + l2.val + carry;
      }

      carry = result<10?0:1;
      result = result - (10*carry);
      current = new ListNode(result);
      if (prev != null) {
        prev.next = current;
      } else {
        root = current;
      }

      return add(l1==null?null:l1.next, l2==null?null:l2.next, carry, current, root);
    }

    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        /*
    (9,4,3) + (5, 6, 4)
    342 + 469

    342
    469
    ---
     8 1 1

    takes a link list and return number (2,4,3) 342


    4!
    4*3*2*1
    fact(n) {
    if n==1
    return 1;
      return n * fact(n-1)
    }

    fact(n, n1) {
    if n==1
    return 1;
      return fact(n-1, n1*n)
    }

    fact (4, 1)

    */
    return add(l1, l2, 0, null, null);


    }


    public static void main(String[] args) {
      ListNode root1 = new ListNode(2);
      root1.next = new ListNode(4);
      root1.next.next = new ListNode(3);

      ListNode root2 = new ListNode(5);
      root2.next = new ListNode(6);
      root2.next.next = new ListNode(4);

      Solution solution = new Solution();

      printer(solution.addTwoNumbers(root1, root2));

    }

    static void printer(ListNode root) {
      while(root != null) {
        System.out.print(root.val + ",");
        root = root.next;
      }
      System.out.println("");
    }
  }
}
